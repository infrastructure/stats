#!/usr/bin/env python3

import argparse
import concurrent.futures
import csv
import fnmatch
import logging
import sys

import osc.conf
import osc.core


def thread_pool(num_workers, func, items, num_retries=0):
    def func_with_retries(item):
        for i in range(num_retries + 1):
            try:
                func(item)
            except Exception as e:
                response_code = getattr(e, "response_code", None)
                itemid = getattr(item, "path_with_namespace", None)
                if itemid is None:
                    itemid = getattr(item, "id", None)
                if itemid is None:
                    itemid = item
                if response_code == 403:
                    print(f"ERROR 403 on item {itemid}, try #{i}", file=sys.stderr)
                    break
                print(f"ERROR on item {itemid}, try #{i}", file=sys.stderr)
                sys.excepthook(*sys.exc_info())
            else:
                break

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        futures = [executor.submit(func_with_retries, item) for item in items]
    results = [f.result() for f in futures]
    return results


class ObsStats:
    def __init__(self):
        self.apiurl = None
        self.projects = None
        self.packages = None

    def connect(self, oscrc=None):
        osc.conf.get_config(override_conffile=oscrc)
        self.apiurl = osc.conf.config["apiurl"]
        logging.info(f'Connecting to the "{self.apiurl}" instance')

    def fetch_projects(self, filterglob=None):
        logging.info("Fetching projects")
        listing = [
            p
            for p in osc.core.meta_get_project_list(self.apiurl)
            if not filterglob or fnmatch.fnmatch(p, filterglob)
        ]
        self.projects = [name for name in listing if not name.endswith(":snapshots")]
        logging.debug(f"Fetched {len(self.projects)} projects")

    def fetch_packages(self):
        logging.info("Fetching packages")
        packages = {}
        num_worker_threads = 10

        def _find_packages(projectname):
            logging.debug(f"Fetching packages from {projectname}")
            listing = osc.core.meta_get_packagelist(self.apiurl, projectname)
            logging.debug(f"Fetched {len(listing)} packages from {projectname}")
            packages[projectname] = listing

        thread_pool(num_worker_threads, _find_packages, self.projects, num_retries=2)
        logging.info(f"Fetched {sum(len(p) for p in packages.values())} packages")
        self.packages = packages

    def dump_packages(self, outfile):
        logging.info(f"Dumping {sum(len(p) for p in self.packages.values())} packages")
        with outfile:
            writer = csv.writer(outfile)
            writer.writerow(["package", "project"])
            for project, packages in self.packages.items():
                for package in packages:
                    row = [package, project]
                    logging.debug(" ".join((str(i) for i in row)))
                    writer.writerow(row)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Collect stats from OBS")
    parser.add_argument(
        "--oscrc", type=str, help="the OSC configuration with the OBS credentials"
    )
    parser.add_argument(
        "--filter",
        type=str,
        help='only search projects matching this glob, like "apertis:v*"',
    )
    parser.add_argument(
        "--packages",
        required=True,
        type=argparse.FileType("w"),
        help="CSV file to store data about packages",
    )
    parser.add_argument(
        "--debug",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        help="print debug information",
    )
    parser.add_argument(
        "--quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
        help="do not print informational output",
    )

    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel or logging.INFO)

    o = ObsStats()
    o.connect(args.oscrc)
    o.fetch_projects(args.filter)
    o.fetch_packages()
    o.dump_packages(args.packages)
